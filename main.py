import sys
from datetime import datetime

from griebnitzsee_mensa.manager import Manager


def parse_requested_refectories():
    ulf = "send_ulf" in str(sys.argv).lower()
    mensa = "send_mensa" in str(sys.argv).lower()

    if not ulf and not mensa:
        ulf = True
        mensa = True

    return ulf, mensa


if __name__ == "__main__test":
    print("ulf is {}, mensa is {}".format(*parse_requested_refectories()))

if __name__ == "__main__":
    print(f"Time: {datetime.now()}")

    send_ulf, send_mensa = parse_requested_refectories()
    manager = Manager(send_ulf, send_mensa)
    error_state = manager.execute()

    if error_state is not None:
        print(error_state.name)
