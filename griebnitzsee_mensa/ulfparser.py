import json
from collections import defaultdict
from datetime import datetime
from json import JSONDecodeError
from typing import List, Dict

import requests

from models.offer import Offer


class UlfParser:
    def __init__(self):
        self._ulf_canteen_id = 112
        self._ulf_url = (
            "https://openmensa.org/api/v2/canteens/{canteen_id}/days/{date}/meals"
        )

    def _download_json(self, date: str) -> List[Dict[str, any]]:
        url = self._ulf_url.format(canteen_id=self._ulf_canteen_id, date=date)
        request = requests.get(url)
        try:
            result = json.loads(request.content)
            return result
        except JSONDecodeError as e:
            print(request.content)
            raise e

    @staticmethod
    def _parse_ulf_json(ulf_json: List[Dict[str, any]]) -> List[Offer]:
        result = []
        default_no_ingredient_dictionary = defaultdict(lambda: False)
        for meal_object in ulf_json:
            result.append(
                Offer(
                    "Ulf's {}".format(meal_object["category"]),
                    meal_object["name"],
                    meal_object.get("prices", {"others": -1.0})["others"],
                    meal_object.get("prices", {"others": -1.0})["others"],
                    default_no_ingredient_dictionary,
                )
            )

        return result

    def get_todays_offers(self) -> List[Offer]:
        return self._parse_ulf_json(
            self._download_json(datetime.today().strftime("%Y-%m-%d"))
        )
