"""Parse the json feed of openmensa for campus kitchen one"""
import json
from collections import defaultdict
from datetime import datetime
from json import JSONDecodeError
from typing import List, Dict

import requests

from models.offer import Offer


class KitchenoneParser:
    """Parser class for campus kitchen one based on UlfParser"""

    def __init__(self):
        self._kitchenone_canteen_id = 1717
        self._kitchenone_url = (
            "https://openmensa.org/api/v2/canteens/{canteen_id}/days/{date}/meals"
        )

    def _download_json(self, date: str) -> List[Dict[str, any]]:
        url = self._kitchenone_url.format(
            canteen_id=self._kitchenone_canteen_id, date=date
        )
        request = requests.get(url, timeout=10)
        try:
            result = json.loads(request.content)
            return result
        except JSONDecodeError as exc:
            print(request.content)
            raise exc

    @staticmethod
    def _parse_kitchenone_json(kitchenone_json: List[Dict[str, any]]) -> List[Offer]:
        result = []
        default_no_ingredient_dictionary = defaultdict(lambda: False)
        for meal_object in kitchenone_json:
            result.append(
                Offer(
                    "Kitchen One's {}".format(meal_object["category"]),
                    meal_object["name"],
                    meal_object.get("prices", {"students": -1.0})["students"],
                    meal_object.get("prices", {"employees": -1.0})["employees"],
                    default_no_ingredient_dictionary,
                )
            )

        return result

    def get_todays_offers(self) -> List[Offer]:
        """Main method: get json, parse it and return offer list"""
        return self._parse_kitchenone_json(
            self._download_json(datetime.today().strftime("%Y-%m-%d"))
        )
