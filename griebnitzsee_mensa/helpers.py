import json
import os
import traceback
from datetime import datetime
from typing import Optional


def _get_file_name(refectory: str) -> str:
    return f"/tmp/{refectory}.json"


def get_last_sent_message(refectory: str) -> Optional[str]:
    if os.path.isfile(_get_file_name(refectory)):
        try:
            with open(_get_file_name(refectory), "r") as f:
                msg = json.load(f)
                if msg["sent_on"] == datetime.today().strftime("%Y-%m-%d"):
                    return msg["message"]
        except Exception:
            print(f"START: Couldn't read last sent message for {refectory}:")
            traceback.print_exc()
            print(f"FINISH: Couldn't read last sent message for {refectory}")


def save_sent_message(refectory: str, message: str) -> str:
    try:
        with open(_get_file_name(refectory), "w") as f:
            msg = {"sent_on": datetime.today().strftime("%Y-%m-%d"), "message": message}
            json.dump(msg, f)
            return _get_file_name(refectory)
    except Exception:
        print(f"START: Couldn't save last sent message for {refectory}:")
        traceback.print_exc()
        print(f"FINISH: Couldn't save last sent message for {refectory}:")
